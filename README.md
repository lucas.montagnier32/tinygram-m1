# TinyGram

Nathan DESHAYES, Malo LE RESTE, Lucas MONTAGNIER

Projet de Web and Cloud de Master 1

## Screen des kinds du datastore:

#### Kind Post:

![Kind Post](images/KindPost.png)

#### Kind Likes:

![Kind Likes](images/KindLikes.png)

#### Kind Follows:

![Kind Follows](images/KindFollows.png)

## Benchmark:

#### Mesure de quelques requêtes:

##### Capture d'écran du temps de réponse en millisecondes de plusieurs requêtes sur le deploy du tinygram:

![Capture d'écran du temps de réponse en millisecondes de plusieurs requêtes sur le deploy du tinygram](mesure.png)
